#!/usr/bin/env -S csi -script
;; output formatting
(import (chicken format))

;; object system
(import coops)

(define-class <item> () (name base-value))

;; default product class
(define-class <crop> (<item>) (grow-time price regrow-time))

;; pretty print product
(define-method (print-object (a <crop> ) default-print-object)
               "pretty print crop"
               (display (format "name:~a, base-value:~a, grow-time:~a, price:~a"
                       (slot-value a 'name)
                       (slot-value a 'base-value)
                       (slot-value a 'grow-time)
                       (slot-value a 'regrow-time)
                       (slot-value a 'price))))

(define-class <vegtable> (<crop>) ())
(define-class <fruit> (<crop>) ())
(define-class <flower> (<crop>) ())

;; TODO: make into a method 
;; sometimes this should ignore the seed price
(define (per-day x)
  "Profit per day"
  (print (/ (car x)  (cdr x))))

;; TODO: turn into method
(define (wine x)
  "Turning a fruit into wine"
  (cons
    (* 3.0 (car x)) 
    (+ 6.25 (cdr x))))

;; TODO: turn into method
(define (juice x)
  "Turning a vegtable into juice"
  (cons
    (* 2.25 (car x)) 
    (+ 4 (cdr x))))

(define (cask x)
  "Aging wine to iridium quality"
  (cons 
    (* 2.0 (car x))
    (+ 56 (cdr x))))

(define (iridium x)
  "iridium quality modifier"
  (cons 
    (* 2.0 (car x))
    (cdr x)))

(define (jar x)
  "Turn into a jelly/pickle"
  (cons
    (+ (* 2 (car x)) 50)
    (+ 3 (cdr x))))

(define (artisan-skill x)
  "Skill: Artisan"
  (cons
    (* 1.4 (car x))
    (cdr x)))

(define (tiller-skill x)
  "Skill: Tiller"
  (cons 
    (* 1.1 (car x))
    (cdr x)))


(define amaranth (make <vegtable> 'name "amaranth" 'price 70 'grow-time 7  'base-value 150.))
amaranth
(define ancient-fruit (make <fruit> 'price 1000 'grow-time 28 'base-value 550.)) ;;regrow 7d
(define apple (make <fruit> 'name "apple" 'base-value 100. 'grow-time 28 'price 1000 'regrow-time 1))
(define apricot (cons 50. 1))
(define artichoke (make <vegtable> 'price 30 'grow-time 8 'base-value 160.))
(define banana (cons 150. 1))
(define beet (make <vegtable> 'price 20 'grow-time 6 'base-value 100.)) ;; oasis
(define blackberry (cons 20. 1))
(define blue-jazz (make <flower> 'name "blue jazz" 'base-value 50 'price 30. 'grow-time 7))
(define blueberry (make <fruit> 'name "blueberry" 'base-value 50. 'grow-time 13 'price 80)) ;;regrow 4
(define bok-choy (make <vegtable> 'price 50 'grow-time 4 'base-value 80.))
(define cactus-fruit (make <fruit> 'price 150 'grow-time 12 'base-value 75. 'regrow-time 3)) ;; regrow 3 
(define cauliflower (make <vegtable> 'name "cauliflower" 'base-value 175. 'grow-time 12 'price 80.))
(define cherry (cons 80. 1))
(define coconut (cons 100. 1))
(define coffee-bean (cons 15. 2))
(define corn (make <vegtable> 'name "corn" 'base-value 50. 'grow-time 14 'price 150 )) ;;regrow 4d
(define cranberries (make <fruit> 'price 240 'grow-time 7 'base-value 75.)) ;;regrow 5
(define crystal-fruit (cons 150. 1))
(define eggplant (make <vegtable> 'price 20 'grow-time 5 'base-value 60.)) ;;regrow 5d
(define fairy-rose (make <flower> 'price 200 'grow-time 12 'base-value 290.))
(define garlic (make <vegtable> 'name "garlic" 'base-value 60. 'grow-time 4 'price 40.))
(define grape (make <fruit> 'price 60 'grow-time 10 'base-value 80. )) ;;regrow 3
(define green-bean (make <vegtable> 'name "green bean" 'base-value 40. 'grow-time 3 'price 60))
(define hops (make <vegtable> 'price 60 'base-value 25.  'grow-time 11));;regrow 1d
(define hot-pepper (make <vegtable> 'price 40. 'grow-time 5 'base-value 40 ));;regrow 3
(define kale (make <vegtable> 'name "kale" 'base-value 110. 'grow-time 6 'price 70))
(define mango (cons 130. 1))
(define melon (make <fruit> 'base-value 250. 'price 80 'grow-time 12))
(define orange (cons 100. 1))
(define parsnip (make <vegtable> 'name "parsnip" 'base-value 35. 'grow-time 4 'price 20))
(define peach (cons 140. 1))
(define pineapple (make <fruit> 'price 400 'grow-time 14 'base-value 300. )) ;;regrow 7d
(define pommegranate (cons 140. 1))
(define poppy (make <flower> 'base-value 140. 'price 100 'grow-time 7))
(define potato (make <vegtable> 'name "potato" 'base-value 80. 'grow-time 6 'price 50.))
(define pumpkin (make <vegtable> 'price 100 'grow-time 13 'base-value 320.))
(define qi-fruit (cons 1. 1))
(define radish (make <vegtable> 'price 40. 'grow-time 6 'base-value 90.))
(define red-cabbage (make <vegtable> 'price 100 'grow-time 9 'base-value 260.))
(define rhubarb (make <fruit> 'name "rhubarb" 'base-value 220. 'grow-time 13 'price 100))
(define salmonberry (cons 5. 1))
(define spice-berry (cons 80. 1))
(define starfruit (make <fruit> 'price 400 'grow-time 13 'base-value 750.)) ;; oasis 
(define strawberry (make <fruit> 'name "strawberry" 'base-value 120. 'price 100 'grow-time 8)) ;;regrow 4d
(define summer-spangle (make <flower> 'price 50 'base-value 90. 'grow-time 8))
(define sunflower (make <flower> 'price 200 'grow-time 8 'base-value 80.))
(define sweet-gem-berry (make <fruit> 'price 1000 'grow-time 24 'base-value 3000.))
(define tea (make <vegtable> 'price 0 'grow-time 20 'base-value 50. )) ;;regrow 1d
(define taro-root (make <vegtable> 'price 24 'grow-time 7 'base-value 100))
(define tomato (make <vegtable> 'price 50 'base-value 60. 'grow-time 11)) ;;regrow 4d
(define tulip (make <flower> 'name "tulip" 'base-value 30. 'grow-time 6 'price 20))
(define unmilled-rice (make <vegtable> 'name "unmilled rice" 'base-value 30. 'grow-time 6 'price 40))
(define wheat (make <vegtable> 'price 10 'grow-time 4 'base-value 25.))
(define wild-plum (cons 80. 1))
(define yam (make <vegtable> 'price 60 'grow-time 10 'base-value 160.))
(define beer (cons 200. 1)) ; needs wheat time added
(define coffee (cons 150 0.12)) ; needs coffee bean time added. 2h
(define green-tea (cons 100 0.18)) ; needs tea leaves. 3h
(define mead (cons 200 0.1)) ; needs honey. 10h
(define pale-ale (cons 300 0.9)); needs hops. 2250 mins
;; truffle
;; truffle oil
;; egg
;; milk
;; cheese
;; mayo

